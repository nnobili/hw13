package Wx;

import java.net.URL;
import java.util.ResourceBundle;

//import ch.makery.wx.model.WxModel;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;


public class WxController implements Initializable {

  @FXML
  private Button btngo;

  @FXML
  private TextField txtzipcode;

  @FXML
  private Label lblcity;
  
  @FXML
  private Label lbltime;
  
  @FXML
  private Label lblweather;

  @FXML
  private Label lbltemp;
  
  @FXML
  private Label lblwind;
  
  @FXML
  private Label lblpress;
  
  @FXML
  private Label lblvis;

  @FXML
  private ImageView iconwx;
  
  @FXML
  private void handleButtonAction(ActionEvent e) {
    // Create object to access the Model
    WxModel weather = new WxModel();

    // Has the go button been pressed?
    if (e.getSource() == btngo)
    {
      String zipcode = txtzipcode.getText();
      if (weather.getWx(zipcode)==true)
      {
        lblcity.setText(weather.getLocation());
        lbltime.setText(weather.getTime());
        lblweather.setText(weather.getWeather());
        lbltemp.setText(String.valueOf(weather.getTemp())+ " F");
        lblwind.setText(weather.getWind());
        lblpress.setText(String.valueOf(weather.getPressure())+" inHG");
        lblvis.setText(String.valueOf(weather.getVisibility())+" miles");
        iconwx.setImage(weather.getImage());
      }
      else
      {
        lblcity.setText("Invalid Zipcode");
        lbltime.setText("");
        lblweather.setText("");
        lbltemp.setText("");
        lblwind.setText("");
        lblpress.setText("");
        lblvis.setText("");
      }
    }
  }

  @Override
  public void initialize(URL url, ResourceBundle rb) {
    // TODO
  }    

}
